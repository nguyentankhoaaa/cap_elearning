import React from 'react'
import { NavLink } from 'react-router-dom'

export default function ItemCourse({item}) {
  return (
   
<NavLink onClick="scroll(0,0)" to={`/detail/${item.maKhoaHoc}`}>
<div className="item-course mt-3 rounded-sm bg-white relative  shadow-2xl ">
  <a href="#">
    <img className="rounded-xl  w-full h-48 object-contain" src={item.hinhAnh}  />
  </a>
  <div className="p-5 ">
    <a href="#">
      <h5 style={{top:"169px"}} className=" text-base
      left-0  font-sans bg-green-600 px-1 rounded-r-lg tracking-tight text-white absolute">{item.tenKhoaHoc}</h5>
    </a>
    <p className="mb-1 font-normal text-gray-700 dark:text-black">Chuyên ngành công nghệ phần mềm là gì ( Công nghệ t...</p>
 <div className='grid grid-cols-2'>

 <div className='flex items-center  '>
  <i className="fa fa-user-circle text-red-500 text-2xl" />  
  <p className='ml-1 text-sm'>{item.nguoiTao.hoTen}</p>
  </div>

 <div className='ml-12 flex  text-gray-700   items-center'>
<i className="fa fa-eye" />

<span className='ml-1'>{item.luotXem}</span>
</div>
 </div>
 
  <hr className='w-full mt-4 bg-gray-600 ' style={{height:"1px"}} />

  <div className='grid grid-cols-2  mt-2'>
    <div className='relative'>
      <p className='text-xs line-through text-gray-500'>800.000 <span className='absolute bottom-7'>đ</span></p>
      <p className='text-base font-medium  text-green-600'>500.000 <span className='absolute bottom-2'>đ</span></p>
    </div>
  <div className='ml-5 mt-2'>
  <p  className='text-yellow-500'><i className="fa fa-star" /> <span>4.9</span><span  className='text-xs  text-gray-500'>(7840)</span>
</p>
  </div>
  </div>
  </div>
</div>
</NavLink>


  )
}
