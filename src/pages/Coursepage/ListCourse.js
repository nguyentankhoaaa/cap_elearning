import React, { useEffect, useState } from 'react'
import { courceServ } from '../../service/courseService'
import ItemCourse from './ItemCourse';
import ReactDOM from 'react-dom';
import ReactPaginate from 'react-paginate';

export default function ListCourse() {
  const [itemOffset, setItemOffset] = useState(0);
    const [listCourse, setListCourse] = useState([])
    useEffect(() => {
    courceServ.getListCource().then((res) => {
          console.log(res.data);
         setListCourse(res.data)
          })
          .catch((err) => {
           console.log(err);
          });
        }, [])
        const itemsPerPage = 12;
            const endOffset = itemOffset + itemsPerPage;
            
            const currentItems = listCourse.slice(itemOffset, endOffset);
            const pageCount = Math.ceil(listCourse.length / itemsPerPage);
            const handlePageClick = (event) => {
              const newOffset = (event.selected * itemsPerPage) % listCourse.length;
          
              setItemOffset(newOffset);
            };
    
  return (
<div >
   <h2 className='text-xl font-bold text-gray-700 mb-3 '><i className="fa fa-bookmark  text-sky-400" /> Danh Sách Khóa Học</h2>
    <div className='grid grid-cols-4 gap-12'  >
            {currentItems.map((item,index)=>{
            return <ItemCourse item={item} key={index}/>
         }) }
    </div>
    <ReactPaginate  
        breakLabel="..."
        nextLabel="next >"
        onPageChange={handlePageClick}
        pageRangeDisplayed={5}
        pageCount={pageCount}
        previousLabel="< previous"
        renderOnZeroPageCount={null}
        pageLinkClassName='page-num'
        previousLinkClassName='page-num'
        nextLinkClassName='page-num'
        activeLinkClassName='active'
        containerClassName='pagination'
        
      />

</div>
  )
}
