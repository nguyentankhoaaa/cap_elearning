import React, { useEffect, useState } from 'react'
import { courceServ } from '../../service/courseService'
import { useParams } from 'react-router'
import avatar from '../../assets/img/avatar.jpg'
export default function DetailPage() {
    let {id} = useParams();
    const [infor, setInfor] = useState([])
    useEffect(() => {
        courceServ.getInforCource(id).then((res) => {
            setInfor(res.data)
            console.log(res.data);
      
          })
          .catch((err) => {
           console.log(err);
          });
        }, [])   
  return (
    <div className='detailPage'>
          <div className='item-name bg-gray-600 text-white px-10 py-10'>
                 <h2 className='text-3xl font-semibold'>THÔNG TIN KHÓA HỌC</h2>
                 <p className='text-sm font-sans'>TIẾN LÊN VÀ KHÔNG CHẦN CHỪ !!</p>
        </div>
        <div  className=' mt-10 detail-container px-14 flex justify-between '>
           <div className='w-2/3 detail-left'>
    <div  style={{borderBottom:"1px solid #d0d0d0"}}  className='first-item'>
  <h1 className='font-medium text-2xl'>{infor.tenKhoaHoc}</h1>
  <div className='flex justify-between mt-10 '>
<div className='col-1  giangVien flex justify-center items-center'>
<div className='image'>
<img src={avatar} alt="" className='w-14 h-12 rounded-full' />
</div>
<div className='title ml-1'>
<h1 className='text-gray-400 font-medium text-sm'>Giảng Viên</h1>
<p>Robert Ngô Ngọc</p>
</div>
</div>
<div className='col-2 linhVuc flex justify-center items-center'>
<div className='image'>
<i className='fas fa-graduation-cap text-cyan-500 text-4xl'></i>
</div><div className='title ml-1'>
<h1 className='text-gray-400 font-medium text-sm'>Lĩnh Vực</h1>
<p>{infor.tenKhoaHoc}</p>
</div>
</div>
<div className='col-3 danhGia '>
<p className='text-xl'><span><i className="fa fa-star text-yellow-400" /></span>
  <span><i className="fa fa-star text-yellow-400" /></span>
  <span><i className="fa fa-star text-yellow-400" /></span>
  <span><i className="fa fa-star text-yellow-400" /></span>
  <span><i className="fas fa-star-half-alt text-yellow-400 " /></span>
  <span className='font-bold text-lg'>4.5</span></p>
  <p className='text-gray-400 font-medium text-sm'>380 đánh giá</p>

</div>
  </div>
  <div className='content my-6  text-gray-500 font-medium'>
<p className='text-justify'>
React.js là thư viện JavaScript phổ biến nhất mà bạn có thể sử dụng và tì
m hiểu ngày nay để xây dựng giao diện người dùng hiện đại, phản ứng cho web.
Khóa học này dạy bạn về React chuyên sâu, từ cơ bản, từng bước đi sâu vào tất
 cả các kiến ​​thức cơ bản cốt lõi, khám phá rất nhiều ví dụ và cũng giới thiệu 
 cho bạn các khái niệm nâng cao.Bạn sẽ nhận được tất cả lý thuyết, hàng tấn ví 
 dụ và bản trình diễn, bài tập và bài tập cũng như vô số kiến ​​thức quan trọng 
 bị hầu hết các nguồn khác bỏ qua - sau cùng, có một lý do tại sao khóa học này
  lại rất lớn! Và trong trường hợp bạn thậm chí không biết tại sao bạn lại muốn
   học React và bạn chỉ ở đây vì một số quảng cáo hoặc "thuật toán" - đừng lo 
   lắng: ReactJS là một công nghệ quan trọng với tư cách là một nhà phát triển 
   web và trong khóa học này,
 tôi sẽ cũng giải thích TẠI SAO điều đó lại quan trọng!
</p>
  </div>
    </div>
    <div className='second-item py-5'> 
<h1 className='font-semibold text-xl' >Những gì bạn sẽ học</h1>
<div className='grid grid-cols-2'>
<ul className='col-1'>
<li className='mt-3'>
  <i className='fa fa-check text-yellow-500  '></i> <span>Xây dựng các ứng dụng web mạnh mẽ, nhanh chóng, thân thiện với người dùng và phản ứng nhanh</span>
</li>
<li className='mt-3'>
  <i className='fa fa-check  text-yellow-500'></i> <span>Đăng ký công việc được trả lương cao hoặc làm freelancer trong một trong những lĩnh vực được yêu cầu nhiều nhất mà bạn có thể tìm thấy trong web dev ngay bây giờ</span>
</li>
<li className='mt-3'>
  <i className='fa fa-check  text-yellow-500'></i> <span>Cung cấp trải nghiệm người dùng tuyệt vời bằng cách tận dụng sức mạnh của JavaScript một cách dễ dàng</span>
</li>
<li className='mt-3'>
  <i className='fa fa-check  text-yellow-500'></i> <span>Tìm hiểu tất cả về React Hooks và React Components</span>
</li>
</ul>
<ul className='col-2'>
<li className='mt-3'>
  <i className='fa fa-check text-yellow-500  '></i> <span>Thông thạo chuỗi công cụ hỗ trợ React, bao gồm cú pháp Javascript NPM, Webpack, Babel và ES6 / ES2015</span>
</li>
<li className='mt-3'>
  <i className='fa fa-check  text-yellow-500'></i> <span>Nhận ra sức mạnh của việc xây dựng các thành phần có thể kết hợp</span>
</li>
<li className='mt-3'>
  <i className='fa fa-check  text-yellow-500'></i> <span>Hãy là kỹ sư giải thích cách hoạt động của Redux cho mọi người, bởi vì bạn biết rất rõ các nguyên tắc cơ bản</span>
</li>
<li className='mt-3'>
  <i className='fa fa-check  text-yellow-500'></i> <span>Nắm vững các khái niệm cơ bản đằng sau việc cấu trúc các ứng dụng Redux</span>
</li>
</ul>
</div>
    </div>
    <div className='third-item py-5'> 
<h1 className='font-semibold text-xl' >Nội dung khóa học</h1>
<div className="item-1 mt-8">
<h1 className='bg-gray-100 px-2 py-3 font-sans text-2xl'>MỤC 1: GIỚI THIỆU 
<span><button >XEM TRƯỚC</button></span></h1>

</div>

    </div>
    
            </div> 
            <div className='w-1/3 detail-right ml-10'>
<div style={{boxShadow:"1px 1px 10px 4px #dadada"}} className="  w-full bg-white  rounded-lg  dark:bg-gray-800 dark:border-gray-700">
  <a href="#">
    <img className="rounded-t-lg  w-full h-64 object-fill" src={infor.hinhAnh} />
  </a>
  <div className="py-5 bg-white px-8">
    <a href="#" className='relative '>
      <h5 className="text-right mb-2 text-2xl font-medium bg-white text-black py-3">
        <i className="fa fa-coins text-yellow-500 mr-2" /> 500.000 <span className='absolute top-0 text-base'>đ</span></h5>
    </a>
   <button className='btn-dangKy rounded-lg w-full border-2 font-medium border-cyan-500 text-cyan-500 py-2
   '>ĐĂNG KÝ</button>
   <nav>
<div className='flex justify-between items-center'>
<p className='text-gray-500 py-5 px-2 border-gray-100 border-b-2'>Ghi danh: <span className='text-black font-medium'>30 học viên</span></p>
<i className='fas fa-user-graduate text-yellow-500 text-xl '></i>
</div>
<div className='flex justify-between items-center'>
<p className='text-gray-500 py-5 px-2 border-gray-100 border-b-2'>Thời gian: <span className='text-black font-medium'>18 giờ</span></p>
<i className=' text-yellow-500 text-xl '></i>
<i className='fa fa-clock text-yellow-500 text-xl '></i>
</div>
<div className='flex justify-between items-center'>
<p className='text-gray-500 py-5 px-2 border-gray-100 border-b-2'>Bài học: <span className='text-black font-medium'>10 </span></p>
<i className='fas fa-book text-yellow-500 text-xl '></i>
</div>
<div className='flex justify-between items-center'>
<p className='text-gray-500 py-5 px-2 border-gray-100 border-b-2'>Video: <span className='text-black font-medium'>14</span></p>
<i className='fas fa-photo-video text-yellow-500 text-xl '></i>
</div>
<div className='flex justify-between items-center'>
<p className='text-gray-500 py-5 px-2 border-gray-100 border-b-2'>Trình độ: <span className='text-black font-medium'>Người mới bắt đầu</span></p>
<i className='fa fa-level-up-alt text-yellow-500 text-xl '></i> 
</div>
<input type="text" className='w-full border-2 border-gray-300 p-1 focus-visible:outline-none rounded-lg' placeholder='Nhập mã' />
   </nav>
  
  </div>
</div>


            </div>

        </div>
    </div>
  )
}
