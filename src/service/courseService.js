import { https } from "./config"

export  const courceServ = {
getListCource:()=>{
    return https.get("/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP01")
},
getInforCource:(id)=>{
    return https.get(`/api/QuanLyKhoaHoc/LayThongTinKhoaHoc?maKhoaHoc=${id}`)
}
}