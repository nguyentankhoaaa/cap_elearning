
import {  https } from "./config"

export const userServ ={
    postLogin: (loginForm)=>{
        return  https.post("/api/QuanLyNguoiDung/DangNhap",loginForm)
    },
    postRegister: (registerForm)=>{
        return https.post("/api/QuanLyNguoiDung/DangKy",registerForm)
    },
}