import React from 'react';
import Lottie from 'lottie-react';
import successNotify from '../../assets/lottie/successNotify.json';
import { useSelector } from 'react-redux';

export default function SuccessNotify() {
    const { isSuccess, massage } = useSelector((state) => state.successNotifySlice);

    const renderContent = () => {
        return isSuccess ? (
            <div className="flex items-center justify-center h-screen w-screen z-50 left-0 top-0 fixed bg-[#9994]">
                <div className="box bg-white rounded-md h-[200px] w-[400px] flex flex-col items-center justify-between">
                    <div className="title h-24 w-24 py-5">
                        <Lottie animationData={successNotify} />
                    </div>
                    <div className="content py-5 text-[27px] font-semibold text-[#41b294]">
                        <span>Đã {massage} thành công!</span>
                    </div>
                </div>
            </div>
        ) : (
            <></>
        );
    };

    return <>{renderContent()}</>;
}
