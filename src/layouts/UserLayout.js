import React from 'react'
import Footer from '../components/Footer/Footter'

export default function UserLayout({Component}) {
  return (
    <div>
      <Component/>
      <Footer/>
    </div>
  )
}
