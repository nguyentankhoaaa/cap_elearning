import React from 'react';

export default function AdminLayout({ Component }) {
    return (
        <div>
            <Component />
        </div>
    );
}
