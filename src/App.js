import './App.css';
import { useDispatch } from 'react-redux';
import Loading from './components/Loading/Loading';
import SuccessNotify from './components/SuccessNotify/SuccessNotify';
import WarningNotify from './components/WarningNotify/WarningNotify';
import { setSuccessOff, setSuccessOn } from './react-toolkit/successNotifySlice';
import { setLoadingOn } from './react-toolkit/loadingSlice';
import { setWarningOff, setWarningOn } from './react-toolkit/warningNotifySlice';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { routes } from './routes/routes';

function App() {
    //demo
    // const dispatch = useDispatch();
    //poup hiện lên xong tắt sau 5s
    // const handleSuccess = () => {
    //     dispatch(setSuccessOn('xóa')); truyền cho nó 1 payload, ví dụ: đã đăng ký thành công => truyền:'đăng ký'
    //     setTimeout(() => {
    //         dispatch(setSuccessOff());
    //     }, 5000);
    // };
    // const handleLoading = () => {
    //     dispatch(setLoadingOn());
    // };
    // const handleWarning = () => {
    //     dispatch(setWarningOn());
    //     setTimeout(() => {
    //         dispatch(setWarningOff());
    //     }, 5000);
    // };

    return (
        <>
            {/* demo */}
            {/* <button onClick={handleLoading}>show loading</button>
            <button onClick={handleSuccess}>show success</button>
            <button onClick={handleWarning}>show warning</button> */}
            <Loading />
            <SuccessNotify />
            <WarningNotify />

            <BrowserRouter>
                <Routes>
                    {routes?.map(({ path, component }) => {
                        return <Route path={path} element={component} />;
                    })}
                </Routes>
            </BrowserRouter>
        </>
    );
}

export default App;
