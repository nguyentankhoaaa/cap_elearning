export const listLogoSourseData = [
    {
        name:"CHƯƠNG TRÌNH HỌC",
        icon:<i class="fa fa-laptop"></i>,
        num:"300",
       
        color:"bg-cyan-500",
    },
    {
        name:"NHÀ SÁNG TẠO",
        icon:<i class="fa fa-camera"></i>,
        num:"10000",
        color:"bg-cyan-600",
       
    },
    {
        name:"NHÀ THIẾT KẾ",
        icon:<i class="fa fa-briefcase"></i>,
        num:"400",
        color:"bg-slate-500",
    },
    {
        name:"BÀI GIẢNG",
        icon:<i class="fa fa-book"></i>,
        num:"3000",
        color:"bg-slate-600",
    },
    {
        name:"VIDEO",
        icon:<i class="fa fa-play-circle"></i>,
        num:"4000",
        color:"bg-sky-600",
    },
    {
        name:"LĨNH VỰC",
        icon:<i class="fab fa-atlassian"></i>,
        num:"200",
        color:"bg-sky-700",
    },
]