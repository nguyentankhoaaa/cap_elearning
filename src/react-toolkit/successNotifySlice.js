import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    isSuccess: false,
    massage: '',
};

const successNotifySlice = createSlice({
    name: 'successNotifySlice',
    initialState,
    reducers: {
        setSuccessOn: (state, action) => {
            state.isSuccess = true;
            state.massage = action.payload;
        },
        setSuccessOff: (state, action) => {
            state.isSuccess = false;
        },
    },
});

export const { setSuccessOn, setSuccessOff } = successNotifySlice.actions;

export default successNotifySlice.reducer;
