import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    isWarning: false,
};

const warningNotifySlice = createSlice({
    name: 'warningNotifySlice',
    initialState,
    reducers: {
        setWarningOn: (state, action) => {
            state.isWarning = true;
        },
        setWarningOff: (state, action) => {
            state.isWarning = false;
        },
    },
});

export const { setWarningOn, setWarningOff } = warningNotifySlice.actions;

export default warningNotifySlice.reducer;
