import AdminLayout from '../layouts/AdminLayout';
import UserLayout from '../layouts/UserLayout';
import AdminCoursesPage from '../pages/AdminCoursesPage/AdminCoursesPage';
import AdminUsersPage from '../pages/AdminUsersPage/AdminUsersPage';
import Coursepage from '../pages/Coursepage/Coursepage';
import DetailPage from '../pages/DetailPage/DetailPage';
import LoginPage from '../pages/LoginPage/LoginPage';
import RegisterPage from '../pages/RegisterPage1/RegisterPage';




export const routes = [
    {
        path: '/admin',
        component: <AdminLayout Component={AdminUsersPage} />,
    },

    {
        path: '/admin/quanlynguoidung',
        component: <AdminLayout Component={AdminUsersPage} />,
    },

    {
        path: '/admin/quanlykhoahoc',
        component: <AdminLayout Component={AdminCoursesPage} />,
    },
    {
        path: '/login',
        component: <LoginPage/>,
    },
    {
        path: '/register',
        component: <RegisterPage/> ,
    },
    {
        path: '/course',
        component: <UserLayout Component={Coursepage} />,
    },
    {
        path: '/detail/:id',
        component: <UserLayout Component={DetailPage} />,
    },
];
